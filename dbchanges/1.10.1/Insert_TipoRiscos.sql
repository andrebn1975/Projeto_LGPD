USE [LGPD]
GO

INSERT INTO [dbo].[TipoRiscos]
           ([tipo_risco]
           ,[data_criacao])
     VALUES
           ('Aus�ncia de crit�rio espec�fico de atribui��o de sigilo','2021-10-20'),
		   ('Aus�ncia de crit�rio sobre obter o M�nimo Necess�rio de Informa��es Pessoais','2021-10-20'),
		   ('Aus�ncia de politica formal de acesso � massa de dados dos sitemas do TCMRJ','2021-10-20'),
		   ('Aus�ncia de Pol�tica Formal de C�pia de Seguran�a','2021-10-20'),
		   ('Contratos em vigor com poss�vel vulnerabilidade de dados pessoais','2021-10-20'),
		   ('Dados Fora da Rede Corporativa','2021-10-20'),
		   ('Dados Pessoais n�o s�o Criptografados','2021-10-20'),
		   ('Descarte de Dados sem Protocolo de Seguran�a','2021-10-20'),
		   ('Insufici�ncia de politica de acesso a dados pessoais custodiados no TCMRJ','2021-10-20'),
		   ('Insufici�ncia de Supervis�o nas �reas Seguras de TI','2021-10-20'),
		   ('Insufici�ncia no Controle de Integridade dos Dados','2021-10-20'),
		   ('Local de Processamento de Dados com Vulnerabilidade','2021-10-20'),
		   ('N�o h� canal criptografado para compartilhamento ou transfer�ncia de dados pessoais','2021-10-20'),
		   ('N�o h� notifica��o aos titulares de dados quando da altera��o','2021-10-20'),
		   ('N�o h� prazo definido para conserva��o de dados','2021-10-20'),
		   ('N�o h� procedimento padronizado quanto ao local de armazenamento','2021-10-20'),
		   ('N�o h� provimento de meios de autentica��o forte para o processamento dos dados pessoais','2021-10-20'),
		   ('Medida Preventiva','2021-10-20')

GO


