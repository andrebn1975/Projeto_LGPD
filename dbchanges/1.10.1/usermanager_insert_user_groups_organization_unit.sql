USE [CSLIB_UserManager]
GO

DECLARE @appId int = (SELECT [Id] FROM Apps WHERE [Name] LIKE 'Django - LGPD') -- LGPD

DECLARE @group_usuario_padrao_id int = (SELECT Id FROM Groups WHERE [Name] LIKE 'Usu�rio Padr�o' AND AppId = @appId)  -- Usu�rio Padr�o
DECLARE @group_usuario_comissao_id int = (SELECT Id FROM Groups WHERE [Name] LIKE 'Comiss�o' AND AppId = @appId) -- Comiss�o
DECLARE @group_super_usuario_id int = (SELECT Id FROM Groups WHERE [Name] LIKE 'Super Usu�rio' AND AppId = @appId) -- Super Usu�rio


-- Adiciona funcion�rios no grupo Super Usu�rio

DECLARE @super_user_1 int = 902081 -- Matheus
DECLARE @super_user_2 int = 901830 -- Andr�
DECLARE @super_user_3 int = 902207 -- Marciel
DECLARE @super_user_4 int = 902206 -- Regis
DECLARE @super_user_5 int = 901761 -- Scotto
DECLARE @super_user_6 int = 901679 -- CARLOS FERNANDO DAS CHAGAS
DECLARE @super_user_7 int = 901028 -- CARLOS ALBERTO BORGES DELGADO JUNIOR
DECLARE @super_user_8 int = 900806 -- CLAUDIO SANCHO M�NICA
DECLARE @super_user_9 int = 900727 -- RODOLFO LUIZ PARDO DOS SANTOS


IF NOT EXISTS (SELECT * FROM [dbo].[UserGroupOrganizationUnit] 
				WHERE [Users_Id] IN (
						@super_user_1, 
						@super_user_2, 
						@super_user_3, 
						@super_user_4,
						@super_user_5,
						@super_user_6,
						@super_user_7,
						@super_user_8,
						@super_user_9
					)
				AND [Groups_Id] = @group_super_usuario_id)
BEGIN
INSERT INTO [CSLIB_UserManager].[dbo].[UserGroupOrganizationUnit]
           ([Users_Id]
           ,[Groups_Id])
     VALUES
           (@super_user_1, @group_super_usuario_id),
		   (@super_user_2, @group_super_usuario_id),
		   (@super_user_3, @group_super_usuario_id),
		   (@super_user_4, @group_super_usuario_id),
		   (@super_user_5, @group_super_usuario_id),
		   (@super_user_6, @group_super_usuario_id),
		   (@super_user_7, @group_super_usuario_id),
		   (@super_user_8, @group_super_usuario_id),
		   (@super_user_9, @group_super_usuario_id)
END


-- Adiciona funcion�rios no grupo Comiss�o

DECLARE @comissao_user_1 int = 000000 -- comissao_user_1

IF NOT EXISTS (SELECT * FROM [dbo].[UserGroupOrganizationUnit] 
				WHERE [Users_Id] IN (
						@comissao_user_1
					)
				AND [Groups_Id] = @group_usuario_comissao_id)
BEGIN
INSERT INTO [CSLIB_UserManager].[dbo].[UserGroupOrganizationUnit]
           ([Users_Id]
           ,[Groups_Id])
     VALUES
           (@comissao_user_1, @group_usuario_comissao_id)
END

-- Adiciona funcion�rios no grupo Usu�rio Padr�o

DECLARE @user_padrao_1 int = 000000 -- user1

IF NOT EXISTS (SELECT * FROM [dbo].[UserGroupOrganizationUnit] 
				WHERE [Users_Id] IN (
						@user_padrao_1
					)
				AND [Groups_Id] = @group_usuario_padrao_id)
BEGIN
INSERT INTO [CSLIB_UserManager].[dbo].[UserGroupOrganizationUnit]
           ([Users_Id]
           ,[Groups_Id])
     VALUES
           (@user_padrao_1, @group_usuario_padrao_id)
END