USE [CSLIB_UserManager]
GO

--select MAX(Id) as GroupsId from Groups 
DECLARE @appId int = (SELECT [Id] FROM Apps WHERE [Name] LIKE 'Django - LGPD') -- LGPD

-- Cria o Grupo Usu�rio Padr�o
DECLARE @group_usuario_padrao_id int = (select MAX(Id) from Groups) + 1;
DECLARE @group_usuario_padrao varchar(50) = 'Usu�rio Padr�o';
IF NOT EXISTS (SELECT * FROM [dbo].[Groups] where [AppId] = @appId and [Name] = @group_usuario_padrao)
BEGIN
INSERT INTO [dbo].[Groups]
           ([Id]
		   ,[Name]
           ,[AppId]
           ,[IsAdmin])
     VALUES
           (@group_usuario_padrao_id, @group_usuario_padrao, @appId, 0)
END

-- Cria o Grupo Usu�rio Comiss�o
DECLARE @group_usuario_comissao_id int = (select MAX(Id) from Groups) + 1;
DECLARE @group_usuario_comissao varchar(50) = 'Comiss�o';
IF NOT EXISTS (SELECT * FROM [dbo].[Groups] where [AppId] = @appId and [Name] = @group_usuario_comissao)
BEGIN
INSERT INTO [dbo].[Groups]
           ([Id]
		   ,[Name]
           ,[AppId]
           ,[IsAdmin])
     VALUES
           (@group_usuario_comissao_id, @group_usuario_comissao, @appId, 0)
END

-- Cria o Grupo Usu�rio Super Usu�rio
DECLARE @group_super_usuario_id int = (select MAX(Id) from Groups) + 1;
DECLARE @group_super_usuario varchar(50) = 'Super Usu�rio';
IF NOT EXISTS (SELECT * FROM [dbo].[Groups] where [AppId] = @appId and [Name] = @group_super_usuario)
BEGIN
INSERT INTO [dbo].[Groups]
           ([Id]
		   ,[Name]
           ,[AppId]
           ,[IsAdmin])
     VALUES
           (@group_super_usuario_id, @group_super_usuario, @appId, 1)
END
