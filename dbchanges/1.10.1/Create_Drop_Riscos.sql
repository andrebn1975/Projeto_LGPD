USE [LGPD]
GO

ALTER TABLE [dbo].[Riscos] DROP CONSTRAINT [Riscos_tipo_risco_id_397f1be3_fk_TipoRiscos_id]
GO

ALTER TABLE [dbo].[Riscos] DROP CONSTRAINT [Riscos_resposta_id_12308200_fk_Respostas_id]
GO

ALTER TABLE [dbo].[Riscos] DROP CONSTRAINT [Riscos_desdobramento_id_b5b0604f_fk_Desdobramentos_id]
GO

/****** Object:  Table [dbo].[Riscos]    Script Date: 20/10/2021 14:37:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Riscos]') AND type in (N'U'))
DROP TABLE [dbo].[Riscos]
GO

/****** Object:  Table [dbo].[Riscos]    Script Date: 20/10/2021 14:37:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Riscos](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[origem_risco] [nvarchar](50) NOT NULL,
	[acao] [nvarchar](50) NOT NULL,
	[instrumento] [nvarchar](50) NOT NULL,
	[apontamento] [nvarchar](max) NOT NULL,
	[resolucao] [nvarchar](max) NULL,
	[data_criacao] [date] NOT NULL,
	[desdobramento_id] [bigint] NULL,
	[resposta_id] [bigint] NULL,
	[tipo_risco_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Riscos]  WITH CHECK ADD  CONSTRAINT [Riscos_desdobramento_id_b5b0604f_fk_Desdobramentos_id] FOREIGN KEY([desdobramento_id])
REFERENCES [dbo].[Desdobramentos] ([id])
GO

ALTER TABLE [dbo].[Riscos] CHECK CONSTRAINT [Riscos_desdobramento_id_b5b0604f_fk_Desdobramentos_id]
GO

ALTER TABLE [dbo].[Riscos]  WITH CHECK ADD  CONSTRAINT [Riscos_resposta_id_12308200_fk_Respostas_id] FOREIGN KEY([resposta_id])
REFERENCES [dbo].[Respostas] ([id])
GO

ALTER TABLE [dbo].[Riscos] CHECK CONSTRAINT [Riscos_resposta_id_12308200_fk_Respostas_id]
GO

ALTER TABLE [dbo].[Riscos]  WITH CHECK ADD  CONSTRAINT [Riscos_tipo_risco_id_397f1be3_fk_TipoRiscos_id] FOREIGN KEY([tipo_risco_id])
REFERENCES [dbo].[TipoRiscos] ([id])
GO

ALTER TABLE [dbo].[Riscos] CHECK CONSTRAINT [Riscos_tipo_risco_id_397f1be3_fk_TipoRiscos_id]
GO


