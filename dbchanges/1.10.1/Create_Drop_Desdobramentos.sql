USE [LGPD]
GO

ALTER TABLE [dbo].[Desdobramentos] DROP CONSTRAINT [Desdobramentos_resposta_id_f44c0b47_fk_Respostas_id]
GO

ALTER TABLE [dbo].[Desdobramentos] DROP CONSTRAINT [Desdobramentos_parent_id_64bf9702_fk_Desdobramentos_id]
GO

/****** Object:  Table [dbo].[Desdobramentos]    Script Date: 20/10/2021 14:36:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Desdobramentos]') AND type in (N'U'))
DROP TABLE [dbo].[Desdobramentos]
GO

/****** Object:  Table [dbo].[Desdobramentos]    Script Date: 20/10/2021 14:36:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Desdobramentos](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[respondeu] [bit] NOT NULL,
	[conformidade] [nvarchar](50) NOT NULL,
	[desdobramento] [nvarchar](50) NOT NULL,
	[data_criacao] [date] NOT NULL,
	[revisado] [bit] NOT NULL,
	[pergunta_adicional] [nvarchar](max) NOT NULL,
	[resposta_adicional] [nvarchar](max) NULL,
	[comentario_adicional] [nvarchar](max) NULL,
	[parent_id] [bigint] NULL,
	[resposta_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Desdobramentos]  WITH CHECK ADD  CONSTRAINT [Desdobramentos_parent_id_64bf9702_fk_Desdobramentos_id] FOREIGN KEY([parent_id])
REFERENCES [dbo].[Desdobramentos] ([id])
GO

ALTER TABLE [dbo].[Desdobramentos] CHECK CONSTRAINT [Desdobramentos_parent_id_64bf9702_fk_Desdobramentos_id]
GO

ALTER TABLE [dbo].[Desdobramentos]  WITH CHECK ADD  CONSTRAINT [Desdobramentos_resposta_id_f44c0b47_fk_Respostas_id] FOREIGN KEY([resposta_id])
REFERENCES [dbo].[Respostas] ([id])
GO

ALTER TABLE [dbo].[Desdobramentos] CHECK CONSTRAINT [Desdobramentos_resposta_id_f44c0b47_fk_Respostas_id]
GO

