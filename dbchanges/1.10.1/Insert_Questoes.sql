USE [LGPD]
GO

TRUNCATE TABLE [dbo].[Questoes]
GO

INSERT INTO [dbo].[Questoes]
           ([assunto_id]
           ,[questao]
           ,[data_criacao])
     VALUES
           (1, 'No contrato, h� a obriga��o do operador de dados pessoais notificar o Controlador em caso de ocorr�ncia de viola��o de dados pessoais? - ISO/IEC 29151:2017', '23-07-2021'),
		   (1, 'No processamento de dados, � utilizado o m�nimo necess�rio de dados pessoais para atingir a finalidade pretendida? - ISO/IEC 29151:2017', '23-07-2021'),
		   (1, 'O Controlador obt�m consentimento (LGPD, art 7�, I) do titular de dados para o tratamento de dados pessoais que n�o se enquadre nas demais hip�teses previstas pelo art. 7� e 11 da LGPD? - ISO/IEC 29151:2017', '23-07-2021'),
		   (1, 'Na revis�o de relat�rios de sua compet�ncia, h� certifica��o/verifica��o de que dados sens�veis e n�o p�blicos, oriundos do LabContas ou de outro conv�nio junto ao TCMRJ, n�o est�o sendo disponibilizados?', '23-07-2021'),
		   (1, 'H� utiliza��o de servi�o de nuvem (ex.: Google Drive) para armazenamento de dados pessoais recebidos pelo setor?', '23-07-2021'),
		   (1, 'Em suas rotinas de trabalho, h� tratamento de dados pessoais? Em caso afirmativo, como s�o armazenados? H� controle de acesso a tais informa��es, por meio de autentica��o e/ou defini��o de n�veis de usu�rio?', '23-07-2021'),
		   (1, 'As transfer�ncias internacionais de dados pessoais s�o realizadas de acordo com o disposto pelo art. 33 da Lei 13.709/2018 (LGPD)? - ISO/IEC 29151:2017', '23-07-2021'),
		   (1, 'Como s�o armazenados e disponibilizados os dados em caso de concess�o de vista ou emiss�o de c�pia de pe�a de processo n�o sigiloso? (RITCMRJ, Art. 139)', '23-07-2021'),
		   (1, 'Como s�o armazenados e disponibilizados os dados que constam em certid�o dos despachos e dos fatos apurados, em caso de pedido, por parte do denunciante? (RITCMRJ, Art. 148)', '23-07-2021'),
		   (1, 'Como s�o disponibilizados os dados objeto de pedido externo de informa��o? Existe algum crit�rio de atribui��o de sigilo a determinadas informa��es?', '23-07-2021'),
		   (2, 'Utilize este espa�o para considera��es finais ou para complementar a resposta a qualquer uma das quest�es anteriores.', '23-07-2021'),
		   (3, 'Os terceiros operadores de dados informaram no contrato sobre a utiliza��o de subcontratos para processar dados pessoais? - ISO/IEC 29151:2017', '23-07-2021'),
		   (3, 'Os titulares de dados pessoais s�o notificados de altera��es na forma de tratamento de dados? - ISO/IEC 29151:2017', '23-07-2021'),
		   (3, 'A rede corporativa � segmentada em dom�nios l�gicos (limitando aos funcion�rios o acesso �s redes e aos servi�os de rede especificamente autorizados a usar), de acordo com cada rede local, atendendo �s necessidades de fornecimento de servi�o p�blico e prote��o da rede corporativa? Refer�ncia: Requisitos m�nimos de seguran�a da informa��o aos �rg�os da Administra��o P�blica federal (GSI/PR, 2.2 Orienta��es T�cnicas, 2017) e ABNT NBR ISO/IEC 27002:2013', '23-07-2021'),
		   (3, 'Existem e s�o executados processos peri�dicos de c�pias de seguran�a das configura��es e sistemas operacionais dos switches e roteadores? Refer�ncia: ABNT ISO/IEC 27002:2013 (item 14.2.4)', '23-07-2021'),
		   (3, 'O acesso externo aos sistemas � provido de meios de seguran�a que protegem a confidencialidade e integridade dos dados trafegados, tais como o uso de VPN? Refer�ncia: Requisitos m�nimos de seguran�a da informa��o aos �rg�os da Administra��o P�blica federal (GSI/PR, 2.2 Orienta��es T�cnicas, 2017) e ABNT ISO/IEC 27002:2013', '23-07-2021'),
		   (4, 'H� utiliza��o de criptografia para a prote��o dos dados sens�veis ou cr�ticos armazenados em dispositivos m�veis, m�dias remov�veis ou em banco de dados? - ABNT NBR ISO/IEC 27002:2013 e ABNT NBR ISO/IEC 27701:2019 ', '23-07-2021'),
		   (4, 'Meios de autentica��o forte s�o providos para o processamento dos dados pessoais, em especial os dados sens�veis (dados de sa�de e demais dados previstos pelo art.5�, II da LGPD)? - ISO/IEC 29151:2017', '23-07-2021'),
		   (4, 'O compartilhamento ou transfer�ncia de dados pessoais � realizado por meio de um canal criptografado e de cifra recomendada pelos s�tios especializados de seguran�a? - ISO/IEC 29151:2017 ', '23-07-2021'),
		   (5, 'Como � feita a cust�dia dos dados oriundos de identifica��o dos respons�veis em   an�lise de contratos, presta��es de contas e demais instrumentos feita pelo N�cleo?', '23-07-2021'),
		   (5, 'Como s�o armazenados e disponibilizados os dados relativos �s sess�es, inclusive as de car�ter reservado (RITCMRJ, Art. 120, � 3�), bem como das sess�es sigilosas (RITCMRJ, Art. 132, � 5�)? ', '23-07-2021'),
		   (5, 'Existe tratamento espec�fico dos dados sens�veis dos processos de pessoal analisados?', '23-07-2021'),
		   (5, 'H� apontamento e informa��es digitais dos jurisdicionados, de demais auditados OU terceiros que n�o estejam na rede do TCMRJ?', '23-07-2021'),
		   (5, 'Onde ficam armazenados os dados oriundos da elabora��o de minutas de certid�es concernentes � posi��o das contas anuais de gest�o do Chefe do Executivo (Delibera��o n� 242, de 27 de junho de 2017, Art. 1� � 4�)?', '23-07-2021'),
		   (5, 'Onde ficam armazenados os dados recebidos em atos/instrumentos n�o previstos no Regimento Interno como de remessa obrigat�ria  (INSTRU��O NORMATIVA N.� 1 DE 31 DE JULHO DE 2019, art. 4�)?', '23-07-2021'),
		   (5, 'Onde ficam armazenados os dados recebidos por meio do Portal do Jurisdicionado INSTRU��O NORMATIVA N.� 1 DE 31 DE JULHO DE 2019, art. 2�)?', '23-07-2021'),
		   (5, 'Onde s�o armazenados os dados que subsidiam a publica��o, no Di�rio Oficial do Munic�pio, de conclus�o de d�bito do respons�vel, inclusive os que constam em pauta especial? (RITCMRJ, Art. 94)', '23-07-2021'),
		   (5, 'Onde s�o armazenados os dados relativos � publica��o, no Di�rio Oficial do Munic�pio, de cita��o de respons�veis para alega��es de defesa, bem como de intima��o, de notifica��o, de comunica��o de dilig�ncia e de rejei��o das alega��es de defesa? (RITCMRJ, Art. 46-A)', '23-07-2021'),
		   (5, 'Como � armazenada a documenta��o de cada auditoria? Em meio digital ou local f�sico pr�prio? S�o armazenados todos os pap�is, ou apenas aqueles que fundamentaram os achados? Por quanto tempo s�o conservados? (ISSAI 100-42; NAGs 4408.9.9; 4408.7.1; e 4408.7.2)', '23-07-2021'),
		   (5, 'Onde ficam armazenados dentro da IGE/CAD os dados dos jurisdicionados relativos � Matriz de planejamento e Matriz de Achados?', '23-07-2021'),
		   (5, 'Onde ficam armazenados dentro da IGE/CAD os dados dos jurisdicionados relativos a Propostas de Encaminhamento? ', '23-07-2021'),
		   (5, 'Onde ficam armazenados dentro da IGE/CAD os dados dos jurisdicionados relativos �s matrizes de responsabiliza��o? ', '23-07-2021'),
		   (5, 'Onde ficam armazenados dentro da SGCE os dados oriundos da an�lise do Relat�rio de Auditoria, cap�tulo sobre �Propostas de Encaminhamento�, com especifica��o dos respons�veis?', '23-07-2021'),
		   (5, 'Onde ficam armazenados dentro da SGCE os dados oriundos da identifica��o dos respons�veis em relat�rios de auditoria, tomada de contas e presta��es de contas?', '23-07-2021'),
		   (5, 'Onde ficam armazenados os cadastros dos editais de licita��o por concorr�ncia e por preg�o (DELIBERA��O N� 242, DE 27 DE JUNHO DE 2017, Art. 1� � 3�)?', '23-07-2021'),
		   (5, 'Onde ficam armazenados os dados no exame de editais de licita��o por concorr�ncia e por preg�o (DELIBERA��O N� 242, DE 27 DE JUNHO DE 2017, Art. 1� � 3�)?', '23-07-2021'),
		   (5, 'Onde ficam armazenados os dados que constam documentos elencados no RITCMRJ, Art. 218?', '23-07-2021'),
		   (5, 'Onde ficam armazenados os dados relativos ao exame de contratos, termos aditivos e instrumentos cong�neres, bem como de conv�nios e demais instrumentos que possam gerar despesa (DELIBERA��O N� 242, DE 27 DE JUNHO DE 2017, Art. 1� � 1�)?', '23-07-2021'),
		   (5, 'Os dados analisados ficam somente nos relat�rios ou a SGCE custodia dados de jurisdicionados? De que forma isso ocorre? ', '23-07-2021'),
		   (5, '� realizada uma an�lise peri�dica sobre os dados coletados, se eles continuam limitados ao m�nimo necess�rio para o atendimento a finalidade? - ISO/IEC 29151:2017 ', '23-07-2021'),
		   (5, 'Como os dados dos jurisdicionados sujeitos a multa s�o coletados e armazenados (planilha e SCP)?', '23-07-2021'),
		   (5, 'H� acompanhamento e armazenamento de dados referentes a sindic�ncias, processos administrativos disciplinares ou correi��es instauradas pelas jurisdicionadas, no sentido de acompanhar atribui��es de responsabilidades? (RITCMRJ, Art. 13).', '23-07-2021'),
		   (5, 'Qual a rotina em caso de proposta de aplica��o aos respons�veis das san��es previstas na Lei n� 3.714/2003 (multas)?', '23-07-2021'),
		   (6, 'As m�dias que cont�m c�pias de seguran�a s�o armazenadas em uma localidade remota (�offsite�), a uma dist�ncia suficiente que garanta sua integridade e disponibilidade contra poss�veis danos advindos de um desastre ocorrido no s�tio prim�rio? - ABNT NBR ISO/IEC 27002:2013 (item 12.3) e ABNT NBR ISO/IEC 27701:2019', '23-07-2021'),
		   (6, '� definida a abrang�ncia dos testes de backup e sua periodicidade, de forma que os testes sejam planejados observando as depend�ncias e relacionamentos entre sistemas, considerando inclusive os ambientes de continuidade de neg�cios, com o objetivo de minimizar a possibilidade de que a aus�ncia de sincronismo entre os dados inviabilize ou dificulte sua recupera��o? - ABNT NBR ISO/IEC 27002:2013 e ABNT NBR ISO/IEC 27701:2019', '23-07-2021'),
		   (6, 'H� uma pol�tica ou norma de backup que aborde os procedimentos operacionais que padronizam os processos de gera��o de c�pias de seguran�a e recupera��o de arquivos, assim como os processos de controle de acesso, armazenamento, movimenta��o e descarte das m�dias que cont�m c�pias de seguran�a? - ABNT NBR ISO/IEC 27002:2013', '23-07-2021'),
		   (6, 'O per�odo de reten��o das c�pias de seguran�a e os requisitos de releitura s�o predefinidos, levando-se em considera��o os requisitos de neg�cio, contratuais, regulamentares ou legais? - ABNT NBR ISO/IEC 27002:2013 e ABNT NBR ISO/IEC 27701:2019', '23-07-2021'),
		   (6, 'Os dados pessoais armazenados/retidos possuem controles de integridade permitindo identificar se os dados foram alterados sem permiss�o? - ISO/IEC 29151:2017', '23-07-2021'),
		   (6, 'S�o realizadas c�pias de seguran�a dos logs de acordo com per�odos de reten��o, que consideram os requisitos de neg�cio, contratuais, regulamentares ou legais? - NC n� 21/IN01/DSIC/GS IPR e ABNT NBR ISO/IEC 27002:2013', '23-07-2021'),
		   (7, 'A institui��o utiliza t�cnicas ou m�todos apropriados para garantir exclus�o ou destrui��o segura de dados pessoais (incluindo originais, c�pias e registros arquivados), de modo a impedir sua recupera��o? - ISO/IEC 29151:2017', '23-07-2021'),
		   (7, 'Como � feito o descarte dos documentos f�sicos? Por quanto tempo os documentos f�sicos s�o arquivados antes do descarte? ', '23-07-2021'),
		   (7, 'H� descarte de documentos digitais?', '23-07-2021'),
		   (8, 'O local que processa as informa��es � restrito somente ao pessoal autorizado?  - NC n� 10/IN01/DSIC/GSIPR e ABNT NBR ISO/IEC 27002:2013 (LabContas)', '23-07-2021'),
		   (8, 'O trabalho nas �reas seguras � supervisionado?  - NC n� 07/IN01/DSIC/GSIP R e ABNT NBR ISO/IEC 27002:2013', '23-07-2021'),
		   (9, '� avaliada a necessidade de permitir que operadores e administradores de banco de dados usem linguagens de consulta, que habilitam recupera��o maci�a automatizada de bases de dados que cont�m dados pessoais? - ISO/IEC 29151:2017', '23-07-2021'),
		   (10, 'Ao fornecer a base de informa��es para �rg�os de pesquisa, os dados pessoais s�o anonimizados ou pseudoanonimizados? - ISO/IEC 29151:2017', '23-07-2021'),
		   (10, 'Os dados pessoais utilizados em ambiente de TDH (teste, desenvolvimento e homologa��o) passaram por um processo de anonimiza��o? - ISO/IEC 29151:2017', '23-07-2021'),
		   (1, 'Qual � o fluxo dos dados relativos ao LabContas e demais dados tratados pela CGI?', '21-10-2021')
GO



