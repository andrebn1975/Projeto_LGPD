/****** Script do comando SelectTopNRows de SSMS  ******/
SELECT a.[Id]
      ,[Users_Id]
	  ,c.[Name] as Nome
      ,[Groups_Id]
	  ,b.[Name] as Grupo
      ,[OrganizationUnits_Id]
	  ,[Description]
  FROM [CSLIB_UserManager].[dbo].[UserGroupOrganizationUnit] a
  INNER JOIN [CSLIB_UserManager].[dbo].[Groups] b ON a.Groups_Id = b.Id
  INNER JOIN [CSLIB_UserManager].[dbo].[Users] c ON a.Users_Id = c.Id
  LEFT JOIN [CSLIB_UserManager].[dbo].[OrganizationUnits] d ON a.[OrganizationUnits_Id] = d.Id
  WHERE b.AppId = 63

/*
DELETE FROM [CSLIB_UserManager].[dbo].[UserGroupOrganizationUnit] 
	WHERE [Groups_Id] in (
		SELECT [Id] FROM [CSLIB_UserManager].[dbo].[Groups]
		WHERE AppId = 63
	)
*/
SELECT TOP (1000) [Id]
      ,[Name]
      ,[AppId]
      ,[IsAdmin]
  FROM [CSLIB_UserManager].[dbo].[Groups]
  WHERE AppId = 63

-- DELETE [CSLIB_UserManager].[dbo].[Groups] WHERE AppId = 63

