USE [LGPD]
GO

ALTER TABLE [dbo].[Assuntos] DROP CONSTRAINT [Assuntos_categoria_id_b1a868bc_fk_Categorias_id]
GO

/****** Object:  Table [dbo].[Assuntos]    Script Date: 19/10/2021 14:01:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Assuntos]') AND type in (N'U'))
DROP TABLE [dbo].[Assuntos]
GO

/****** Object:  Table [dbo].[Assuntos]    Script Date: 19/10/2021 14:01:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Assuntos](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[assunto] [nvarchar](max) NOT NULL,
	[data_criacao] [date] NOT NULL,
	[categoria_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Assuntos]  WITH CHECK ADD  CONSTRAINT [Assuntos_categoria_id_b1a868bc_fk_Categorias_id] FOREIGN KEY([categoria_id])
REFERENCES [dbo].[Categorias] ([id])
GO

ALTER TABLE [dbo].[Assuntos] CHECK CONSTRAINT [Assuntos_categoria_id_b1a868bc_fk_Categorias_id]
GO

