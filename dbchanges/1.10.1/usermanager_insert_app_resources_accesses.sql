USE [CSLIB_UserManager]
GO


-- GET APP ID
DECLARE @appId int = (SELECT [Id] FROM Apps WHERE [Name] LIKE 'Django - LGPD') -- LGPD

-- GET LAST APP RESOURCES E ATRIBUI OS RECURSOS
DECLARE @last_app_resource_id int = (select MAX(Id) as AppResourcesId from AppResources)

DECLARE @app_resource_id_contas_categorias int = @last_app_resource_id + 1
DECLARE @app_resource_id_contas_assuntos int = @last_app_resource_id + 2
DECLARE @app_resource_id_contas_questoes int = @last_app_resource_id + 3
DECLARE @app_resource_id_contas_questoesorganizationutint int = @last_app_resource_id + 4
DECLARE @app_resource_id_contas_respostas int = @last_app_resource_id + 5

DECLARE @app_resource_contas_categorias varchar(150) = 'contas.categorias'
DECLARE @app_resource_contas_assuntos varchar(150) = 'contas.assuntos'
DECLARE @app_resource_contas_questoes varchar(150) = 'contas.questoes'
DECLARE @app_resource_contas_questoesorganizationutint varchar(150) = 'contas.questoesorganizationutint'
DECLARE @app_resource_contas_respostas varchar(150) = 'contas.respostas'

-- GET GROUPS ID
DECLARE @group_usuario_padrao_id int = (SELECT Id FROM Groups WHERE [Name] LIKE 'Usu�rio Padr�o' AND AppId = @appId)  -- Usu�rio Padr�o
DECLARE @group_usuario_comissao_id int = (SELECT Id FROM Groups WHERE [Name] LIKE 'Comiss�o' AND AppId = @appId) -- Comiss�o
DECLARE @group_super_usuario_id int = (SELECT Id FROM Groups WHERE [Name] LIKE 'Super Usu�rio' AND AppId = @appId) -- Super Usu�rio

-- GET LAST ACCESSES ID
DECLARE @last_accesses_id int = (SELECT max([Id]) FROM [dbo].[Accesses])

-- Cria o Recurso
IF NOT EXISTS (SELECT * FROM [dbo].[AppResources]
	WHERE [AppId] = @appId
	AND [Description] IN (
		@app_resource_id_contas_categorias,
		@app_resource_contas_assuntos,
		@app_resource_contas_questoes,
		@app_resource_contas_questoesorganizationutint,
		@app_resource_contas_respostas
	)
)
BEGIN
INSERT INTO [dbo].[AppResources]
           ([AppId]
           ,[Id]
           ,[ParentId]
           ,[Description])
     VALUES
		   (@appId, @app_resource_id_contas_categorias, NULL, @app_resource_contas_categorias),
           (@appId, @app_resource_id_contas_assuntos, NULL, @app_resource_contas_assuntos),
		   (@appId, @app_resource_id_contas_questoes, NULL, @app_resource_contas_questoes),
		   (@appId, @app_resource_id_contas_questoesorganizationutint, NULL, @app_resource_contas_questoesorganizationutint),
		   (@appId, @app_resource_id_contas_respostas, NULL, @app_resource_contas_respostas)
END

-- Associa as permiss�es do grupo
BEGIN
INSERT INTO [dbo].[Accesses]
           ([Id]
           ,[AppId]
           ,[ResourceId]
           ,[GroupId]
           ,[CanSelect]
           ,[CanInsert]
           ,[CanUpdate]
           ,[CanDelete]
           ,[CanExecute]
           ,[CanPrint]
           ,[CanSpecial1]
           ,[CanSpecial2]
           ,[CanSpecial3])
     VALUES
		   ((@last_accesses_id + 1),@appId,@app_resource_id_contas_categorias,@group_usuario_padrao_id, 1,0,0,0,0,1,0,0,0), -- usu�rio padr�o - contas.categorias
           ((@last_accesses_id + 2),@appId,@app_resource_id_contas_assuntos,@group_usuario_padrao_id, 1,0,0,0,0,1,0,0,0), -- usu�rio padr�o - contas.assuntos
		   ((@last_accesses_id + 3),@appId,@app_resource_id_contas_questoes,@group_usuario_padrao_id, 1,0,0,0,0,1,0,0,0), -- usu�rio padr�o - contas.questoes
		   ((@last_accesses_id + 4),@appId,@app_resource_id_contas_questoesorganizationutint,@group_usuario_padrao_id, 0,0,0,0,0,1,0,0,0), -- usu�rio padr�o - contas.questoesorganizationutint
		   ((@last_accesses_id + 5),@appId,@app_resource_id_contas_respostas,@group_usuario_padrao_id, 1,1,1,0,0,1,0,0,0), -- usu�rio padr�o - contas.respostas

		   ((@last_accesses_id + 6),@appId,@app_resource_id_contas_categorias,@group_usuario_comissao_id, 1,1,1,0,0,1,0,0,0), -- usu�rio Comiss�o - contas.categorias
		   ((@last_accesses_id + 7),@appId,@app_resource_id_contas_assuntos,@group_usuario_comissao_id, 1,1,1,0,0,1,0,0,0), -- usu�rio Comiss�o - contas.assuntos
		   ((@last_accesses_id + 8),@appId,@app_resource_id_contas_questoes,@group_usuario_comissao_id, 1,1,1,0,0,1,0,0,0), -- usu�rio Comiss�o - contas.questoes
		   ((@last_accesses_id + 9),@appId,@app_resource_id_contas_questoesorganizationutint,@group_usuario_comissao_id, 1,1,1,1,0,1,0,0,0), -- usu�rio Comiss�o - contas.questoesorganizationutint
		   ((@last_accesses_id + 10),@appId,@app_resource_id_contas_respostas,@group_usuario_comissao_id, 1,1,1,0,0,1,0,0,0), -- usu�rio Comiss�o - contas.respostas

		   ((@last_accesses_id + 11),@appId,@app_resource_id_contas_categorias,@group_super_usuario_id, 1,1,1,1,1,1,1,1,1), -- Super Usu�rio - contas.categorias
		   ((@last_accesses_id + 12),@appId,@app_resource_id_contas_assuntos,@group_super_usuario_id, 1,1,1,1,1,1,1,1,1), -- Super Usu�rio - contas.assuntos
		   ((@last_accesses_id + 13),@appId,@app_resource_id_contas_questoes,@group_super_usuario_id, 1,1,1,1,1,1,1,1,1), -- Super Usu�rio - contas.questoes
		   ((@last_accesses_id + 14),@appId,@app_resource_id_contas_questoesorganizationutint,@group_super_usuario_id, 1,1,1,1,1,1,1,1,1), -- Super Usu�rio - contas.questoesorganizationutint
		   ((@last_accesses_id + 15),@appId,@app_resource_id_contas_respostas,@group_super_usuario_id, 1,1,1,1,1,1,1,1,1)  -- Super Usu�rio - contas.respostas
END