USE [LGPD]
GO

/****** Object:  Table [dbo].[Categorias]    Script Date: 19/10/2021 10:21:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Categorias]') AND type in (N'U'))
DROP TABLE [dbo].[Categorias]
GO

/****** Object:  Table [dbo].[Categorias]    Script Date: 19/10/2021 10:21:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Categorias](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[categoria] [nvarchar](255) NOT NULL,
	[data_criacao] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

