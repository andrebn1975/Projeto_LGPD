USE [LGPD]
GO

/****** Object:  Table [dbo].[TipoRiscos]    Script Date: 20/10/2021 14:38:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoRiscos]') AND type in (N'U'))
DROP TABLE [dbo].[TipoRiscos]
GO

/****** Object:  Table [dbo].[TipoRiscos]    Script Date: 20/10/2021 14:38:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TipoRiscos](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[tipo_risco] [nvarchar](255) NOT NULL,
	[data_criacao] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


