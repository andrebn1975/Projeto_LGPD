USE [LGPD]
GO

INSERT INTO [dbo].[Assuntos]
           ([assunto]
           ,[data_criacao]
		   ,[categoria_id])
     VALUES
		   ('Compartilhamento, Uso e Prote��o da Informa��o', '23-07-2021', 3),
           ('Considera��es do setor', '23-07-2021', 3),
		   ('Controle de acesso l�gico', '23-07-2021', 1),
		   ('Controles criptogr�ficos', '23-07-2021', 3),
		   ('Controles de coleta e preserva��o de evid�ncias', '23-07-2021', 1),
		   ('C�pia de Seguran�a', '23-07-2021', 4),
		   ('Descarte de dados', '23-07-2021', 2),
		   ('Prote��o F�sica e do Ambiente', '23-07-2021', 1),
		   ('Registro de eventos e rastreabillidade', '23-07-2021', 3),
		   ('Seguran�a em Redes', '23-07-2021', 3)
GO


