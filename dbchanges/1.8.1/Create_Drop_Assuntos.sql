USE [LGPD]
GO

/****** Object:  Table [dbo].[Assuntos]    Script Date: 28/07/2021 09:21:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Assuntos]') AND type in (N'U'))
DROP TABLE [dbo].[Assuntos]
GO

/****** Object:  Table [dbo].[Assuntos]    Script Date: 28/07/2021 09:21:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Assuntos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[assunto] [nvarchar](max) NOT NULL,
	[data_criacao] [date] NOT NULL,
 CONSTRAINT [PK_Assuntos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


