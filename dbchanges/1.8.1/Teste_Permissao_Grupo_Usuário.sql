/****** Script do comando SelectTopNRows de SSMS  ******/
SELECT TOP (1000) [Id]
      ,[Users_Id]
      ,[Groups_Id]
      ,[OrganizationUnits_Id]
  FROM [CSLIB_UserManager].[dbo].[UserGroupOrganizationUnit] where Groups_Id in (629, 630, 631) ORDER BY Id DESC 
GO

-- UPDATE [CSLIB_UserManager].[dbo].[UserGroupOrganizationUnit] SET Groups_Id = 629 WHERE Id = 13908 --Usu�rio Padr�o
-- UPDATE [CSLIB_UserManager].[dbo].[UserGroupOrganizationUnit] SET Groups_Id = 630 WHERE Id = 13910 --Usu�rio Comiss�o
-- UPDATE [CSLIB_UserManager].[dbo].[UserGroupOrganizationUnit] SET Groups_Id = 631 WHERE Id = 13910 --Super Usu�rio
GO