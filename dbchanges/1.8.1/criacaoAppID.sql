use [CSLIB_UserManager]
GO

-- select MAX(Id) as AppId from Apps 
-- select MAX(Id) as GroupsId from Groups 
-- select MAX(Id) as AppResourcesId from AppResources 

-- APLICACAO
DECLARE @AppId int = (select MAX(Id) as AppId from Apps) + 1

IF NOT EXISTS (SELECT * FROM [dbo].[Apps] where ID = @AppId)
BEGIN
insert into [dbo].[Apps] (Id,Name) values(@AppId, 'Django - LGPD')
END
