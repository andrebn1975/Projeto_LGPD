USE [LGPD]
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_dbLGPD'
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_dbLGPD'
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_dbLGPD'
GO

/****** Object:  View [dbo].[View_dbLGPD]    Script Date: 27/07/2021 10:04:14 ******/
DROP VIEW [dbo].[View_dbLGPD]
GO

/****** Object:  View [dbo].[View_dbLGPD]    Script Date: 27/07/2021 10:04:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_dbLGPD]
AS
SELECT        dbo.Assuntos.id AS assunto_id, dbo.Assuntos.assunto, dbo.Questoes_OrganizationUnit.questao_id, dbo.Questoes.questao, dbo.Questoes_OrganizationUnit.organization_unit_id, 
                         CSLIB_UserManager.dbo.OrganizationUnits.Name AS organization_unit, dbo.Respostas.id AS resposta_id, dbo.Respostas.matricula, dbo.Respostas.resposta, dbo.Respostas.comentario, dbo.Respostas.respondeu, 
                         dbo.Respostas.ano, dbo.Respostas.data_criacao
FROM            dbo.Assuntos INNER JOIN
                         dbo.Questoes ON dbo.Assuntos.id = dbo.Questoes.assunto_id INNER JOIN
                         dbo.Respostas ON dbo.Questoes.id = dbo.Respostas.questao_id INNER JOIN
                         dbo.Questoes_OrganizationUnit ON dbo.Questoes.id = dbo.Questoes_OrganizationUnit.questao_id INNER JOIN
                         CSLIB_UserManager.dbo.OrganizationUnits ON dbo.Questoes_OrganizationUnit.organization_unit_id = CSLIB_UserManager.dbo.OrganizationUnits.Id
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Assuntos"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 138
               Right = 234
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Respostas"
            Begin Extent = 
               Top = 34
               Left = 619
               Bottom = 232
               Right = 825
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Questoes"
            Begin Extent = 
               Top = 188
               Left = 342
               Bottom = 333
               Right = 538
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Questoes_OrganizationUnit"
            Begin Extent = 
               Top = 11
               Left = 327
               Bottom = 141
               Right = 523
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "OrganizationUnits (CSLIB_UserManager.dbo)"
            Begin Extent = 
               Top = 206
               Left = 70
               Bottom = 336
               Right = 266
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_dbLGPD'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'        Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_dbLGPD'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_dbLGPD'
GO


