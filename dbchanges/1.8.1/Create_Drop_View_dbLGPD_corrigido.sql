USE [LGPD]
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_dbLGPD'
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_dbLGPD'
GO

/****** Object:  View [dbo].[View_dbLGPD]    Script Date: 27/07/2021 18:17:22 ******/
DROP VIEW [dbo].[View_dbLGPD]
GO

/****** Object:  View [dbo].[View_dbLGPD]    Script Date: 27/07/2021 18:17:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_dbLGPD]
AS
SELECT        dbo.Assuntos.id AS assunto_id, dbo.Assuntos.assunto, dbo.Respostas.questao_id, dbo.Questoes.questao, dbo.Respostas.organization_unit_id, CSLIB_UserManager.dbo.OrganizationUnits.Name AS organization_unit, 
                         dbo.Respostas.matricula, dbo.Respostas.id AS resposta_id, dbo.Respostas.resposta, dbo.Respostas.comentario, dbo.Respostas.respondeu, dbo.Respostas.ano, dbo.Respostas.data_criacao
FROM            dbo.Assuntos INNER JOIN
                         dbo.Questoes ON dbo.Assuntos.id = dbo.Questoes.assunto_id INNER JOIN
                         dbo.Respostas ON dbo.Questoes.id = dbo.Respostas.questao_id INNER JOIN
                         CSLIB_UserManager.dbo.OrganizationUnits ON dbo.Respostas.organization_unit_id = CSLIB_UserManager.dbo.OrganizationUnits.Id
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[51] 4[10] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Assuntos"
            Begin Extent = 
               Top = 117
               Left = 1583
               Bottom = 249
               Right = 1779
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Questoes"
            Begin Extent = 
               Top = 23
               Left = 977
               Bottom = 168
               Right = 1173
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Respostas"
            Begin Extent = 
               Top = 32
               Left = 57
               Bottom = 230
               Right = 263
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "OrganizationUnits (CSLIB_UserManager.dbo)"
            Begin Extent = 
               Top = 203
               Left = 528
               Bottom = 428
               Right = 978
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1770
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_dbLGPD'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_dbLGPD'
GO

