USE [LGPD]
GO

/****** Object:  Table [dbo].[Questoes_OrganizationUnit]    Script Date: 28/07/2021 09:23:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Questoes_OrganizationUnit]') AND type in (N'U'))
DROP TABLE [dbo].[Questoes_OrganizationUnit]
GO

/****** Object:  Table [dbo].[Questoes_OrganizationUnit]    Script Date: 28/07/2021 09:23:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Questoes_OrganizationUnit](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[questao_id] [int] NOT NULL,
	[organization_unit_id] [int] NOT NULL,
	[ano_pesquisa] [int] NOT NULL,
	[data_criacao] [date] NOT NULL,
 CONSTRAINT [PK_Questoes_OrganizationUnit] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

