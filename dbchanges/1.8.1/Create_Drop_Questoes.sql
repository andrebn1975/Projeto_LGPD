USE [LGPD]
GO

/****** Object:  Table [dbo].[Questoes]    Script Date: 28/07/2021 09:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Questoes]') AND type in (N'U'))
DROP TABLE [dbo].[Questoes]
GO

/****** Object:  Table [dbo].[Questoes]    Script Date: 28/07/2021 09:22:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Questoes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[assunto_id] [int] NOT NULL,
	[questao] [text] NOT NULL,
	[data_criacao] [date] NOT NULL,
 CONSTRAINT [PK_Questoes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

