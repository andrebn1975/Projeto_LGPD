USE [CSLIB_UserManager]
GO

-- Cria o AppID
INSERT INTO [dbo].[Apps]
			([Id]
			,[Name])
	 VALUES
			(63, 'Django - LGPD')

-- Cria o Grupo
INSERT INTO [dbo].[Groups]
           ([Id]
		   ,[Name]
           ,[AppId]
           ,[IsAdmin])
     VALUES
           (639, 'Usu�rio Padr�o', 63, 0),
		   (640, 'Comiss�o', 63, 0),
		   (641, 'Super Usu�rio', 63, 1)
GO

-- Associa o Usu�rio ao Grupo ### Prencher o Id do Usu�rio desejado ###
INSERT INTO [dbo].[UserGroupOrganizationUnit]
           ([Users_Id]
           ,[Groups_Id]
           ,[OrganizationUnits_Id])
     VALUES
           ('902207',639, NULL), -- Marciel - Usu�rio Padr�o
		   ('901830',641, NULL), -- Andr� - Super Usu�rio
		   ('902081',641, NULL), -- Matheus Super Usu�rio
		   ('901761',640, NULL)  -- Scotto - Comiss�o
GO

-- Cria o Recurso
INSERT INTO [dbo].[AppResources]
           ([AppId]
           ,[Id]
           ,[ParentId]
           ,[Description])
     VALUES
           (63,528,NULL,'contas.assuntos'),
		   (63,529,NULL,'contas.questoes'),
		   (63,530,NULL,'contas.questoesorganizationutint'),
		   (63,531,NULL,'contas.respostas')
GO

-- Associa as permiss�es do grupo
INSERT INTO [dbo].[Accesses]
           ([Id]
           ,[AppId]
           ,[ResourceId]
           ,[GroupId]
           ,[CanSelect]
           ,[CanInsert]
           ,[CanUpdate]
           ,[CanDelete]
           ,[CanExecute]
           ,[CanPrint]
           ,[CanSpecial1]
           ,[CanSpecial2]
           ,[CanSpecial3])
     VALUES
           (335,63,528,639, 1,0,0,0,0,1,0,0,0), -- usu�rio padr�o - contas.assuntos
		   (336,63,529,639, 1,0,0,0,0,1,0,0,0), -- usu�rio padr�o - contas.questoes
		   (337,63,530,639, 0,0,0,0,0,1,0,0,0), -- usu�rio padr�o - contas.questoesorganizationutint
		   (338,63,531,639, 1,1,1,0,0,1,0,0,0), -- usu�rio padr�o - contas.respostas

		   (339,63,528,640, 1,1,1,0,0,1,0,0,0), -- usu�rio Comiss�o - contas.assuntos
		   (340,63,529,640, 1,1,1,0,0,1,0,0,0), -- usu�rio Comiss�o - contas.questoes
		   (341,63,530,640, 1,1,1,1,0,1,0,0,0), -- usu�rio Comiss�o - contas.questoesorganizationutint
		   (342,63,531,640, 1,1,1,0,0,1,0,0,0), -- usu�rio Comiss�o - contas.respostas

		   (343,63,528,641, 1,1,1,1,1,1,1,1,1), -- Super Usu�rio - contas.assuntos
		   (344,63,529,641, 1,1,1,1,1,1,1,1,1), -- Super Usu�rio - contas.questoes
		   (345,63,530,641, 1,1,1,1,1,1,1,1,1), -- Super Usu�rio - contas.questoesorganizationutint
		   (346,63,531,641, 1,1,1,1,1,1,1,1,1)  -- Super Usu�rio - contas.respostas
GO
