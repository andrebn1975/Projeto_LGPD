USE [LGPD]
GO

/****** Object:  Table [dbo].[Respostas]    Script Date: 18/08/2021 13:55:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Respostas]') AND type in (N'U'))
DROP TABLE [dbo].[Respostas]
GO

/****** Object:  Table [dbo].[Respostas]    Script Date: 18/08/2021 13:55:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Respostas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[organization_unit_id] [int] NOT NULL,
	[matricula] [nvarchar](10) NULL,
	[questao_id] [int] NOT NULL,
	[resposta] [text] NULL,
	[comentario] [text] NULL,
	[respondeu] [nvarchar](max) NULL,
	[ano] [int] NOT NULL,
	[data_criacao] [date] NOT NULL,
	[revisado] [bit] NULL,
 CONSTRAINT [PK_Respostas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

