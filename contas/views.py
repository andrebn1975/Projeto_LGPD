from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from .filters import RespostasFilter
from .forms import (
    CategoriasForm,
    AssuntosForm, 
    QuestoesForm, 
    RespostasForm, 
    QuestoesOrganizationunitForm, 
    RespostasForm_Comite, 
    RespostasForm_Usuario_Padrao
)

from .models import (
    Categorias, 
    Assuntos, 
    Questoes, 
    Respostas, 
    QuestoesOrganizationunit
)
import datetime
from auth_usermanager.decorators import login_required, permission_required
from django.contrib import messages


@login_required
def home(request):
    return render(request, 'core/home.html')

@login_required
@permission_required('contas.view_categorias', raise_exception=True)
def categorias(request):
    categorias = Categorias.objects.all()
    search = request.GET.get('search')
    if search:
        categorias = categorias.filter(categoria__icontains=search)
    return render(request, 'contas/categorias.html', {'categorias': categorias})
    
@login_required
@permission_required('contas.view_assuntos', raise_exception=True)
def assuntos(request):
    assuntos = Assuntos.objects.all()
    search = request.GET.get('search')
    if search:
        assuntos = assuntos.filter(assunto__icontains=search)
    return render(request, 'contas/assuntos.html', {'assuntos': assuntos})

@login_required
@permission_required('contas.view_questoes', raise_exception=True)
def questoes(request):
    questoes = Questoes.objects.all()
    search_questao = request.GET.get('search')
    if search_questao:
        questoes = questoes.filter(questao__icontains=search_questao).order_by('assunto__assunto')
    return render(request, 'contas/questoes.html', {'questoes': questoes})

@login_required
@permission_required('contas.view_respostas', raise_exception=True)
def respostas(request):
    template_name = 'contas/status.html'
    respostas_search = Respostas.objects.all()
    respostas_search = RespostasFilter(request.GET, queryset=respostas_search)
    context = {'filter': respostas_search}
    return render(request, template_name, context)

@login_required
@permission_required('contas.view_respostas', raise_exception=True)
def respostas_search(request):
    template_name = 'contas/respostas_search.html'
    respostas_search = Respostas.objects.all()
    respostas_search = RespostasFilter(request.GET, queryset=respostas_search)
    context = {'filter': respostas_search}
    return render(request, template_name, context)

@login_required
@permission_required('contas.add_questoesorganizationutint', raise_exception=True)
def atribuicao(request):
    atribuicao = Questoes.objects.all()
    return render(request, 'contas/atribuicao.html', {'atribuicao': atribuicao})

@login_required
@permission_required('contas.add_questoesorganizationutint', raise_exception=True)
def criaratribuicao(request, pk):
    questao = get_object_or_404(Questoes, pk=pk)
    organization_units_questao = QuestoesOrganizationunit.objects.filter(questao_id=pk)
    organization_units_questao_ids = [ouq.organization_unit.id for ouq in organization_units_questao if organization_units_questao]
    form_atribuicao = QuestoesOrganizationunitForm()
    
    if request.method == 'POST':
        form_atribuicao = QuestoesOrganizationunitForm(request.POST)
        if form_atribuicao.is_valid():
            organization_unit = form_atribuicao.cleaned_data['organization_unit']
            if organization_unit.id not in organization_units_questao_ids:
                questao_organization_unit = QuestoesOrganizationunit.objects.create(
                    questao = questao,
                    organization_unit = organization_unit,
                    ano_pesquisa = datetime.datetime.now().date().year
                )
                questao_organization_unit.save()

        return redirect('contas:form_atribuicao', pk=pk)

    context = {
        'form_atribuicao': form_atribuicao, 
        'questao': questao,
        'organization_units_questao': organization_units_questao
    }
    return render(request, 'contas/form_atribuicao.html', context)

@login_required
@permission_required('contas.add_categorias', raise_exception=True)
def criarcategoria(request):
    form_categoria = CategoriasForm()
    if request.method == 'POST':
        form_categoria = CategoriasForm(request.POST)
        if form_categoria.is_valid():
            form_categoria.save()
            messages.add_message(request, messages.SUCCESS, 'Categoria cadastrada com sucesso!')
        return redirect('contas:categorias')
    context = {'form': form_categoria}
    return render(request, 'contas/form_categoria.html', context)
    
@login_required
@permission_required('contas.add_assuntos', raise_exception=True)
def criarassunto(request):
    form_assunto = AssuntosForm()
    if request.method == 'POST':
        form_assunto = AssuntosForm(request.POST)
        if form_assunto.is_valid():
            form_assunto.save()
            messages.add_message(request, messages.SUCCESS, 'Assunto cadastrado com sucesso!')
        return redirect('contas:assuntos')
    context = {'form': form_assunto}
    return render(request, 'contas/form_assunto.html', context)

@login_required
@permission_required('contas.add_questoes', raise_exception=True)
def criarquestao(request):
    form_questao = QuestoesForm()
    if request.method == 'POST':
        form_questao = QuestoesForm(request.POST)
        if form_questao.is_valid():
            form_questao.save()
        return redirect('contas:questoes')
    context = {'form_questao': form_questao}
    return render(request, 'contas/form_questao.html', context)

@login_required
@permission_required('contas.add_respostas', raise_exception=True)
def criarresposta(request, questao_id):
    
    questao = Questoes.objects.get(pk=questao_id)
    organization_unit = request.user.organization_units.all()[0]
    
    # verifica se questão já foi respondida pelo órgão
    has_resposta = Respostas.objects.filter(
        organization_unit=organization_unit,
        questao = questao
    )
    if has_resposta:
        messages.add_message(request, messages.ERROR, 'Questão já respondida anteriomente!')
        #return redirect('contas:respostas')

    if request.user.groups.all()[0].name == 'Comissão':
        form_resposta = RespostasForm_Comite(request.POST,  initial={'questao': questao_id})
        
    elif request.user.groups.all()[0].name == 'Usuário Padrão':
        form_resposta = RespostasForm_Usuario_Padrao(request.POST, initial={'questao': questao_id})
    else:
        form_resposta = RespostasForm(request.POST or None, initial={'questao': questao_id})

    if request.method == 'POST':
        if form_resposta.is_valid(): 
            resposta = form_resposta.save(commit=False)
            resposta.organization_unit = organization_unit
            resposta.matricula = request.user.id
            resposta.ano = datetime.date.today().year
            resposta.save()

            messages.add_message(request, messages.SUCCESS, 'Resposta cadastrada com sucesso!')
            return redirect('contas:respostas')

    context = {
        'form': form_resposta,
        'questao': questao
    }
    return render(request, 'contas/form_resposta.html', context)


@login_required
@permission_required('contas.delete_categorias', raise_exception=True)
def apagarcategoria(request, pk):
    
    categoria = get_object_or_404(Categorias, pk=pk)
    if request.method == "POST":
        categoria.delete()
        messages.add_message(request, messages.SUCCESS, 'Categoria excluída com sucesso!')
        return redirect('contas:categorias')
    context = {'categoria': categoria}
    return render(request, 'contas/apagar_categoria.html', context)

    
@login_required
@permission_required('contas.delete_assuntos', raise_exception=True)
def apagarassunto(request, pk):
    
    assunto = get_object_or_404(Assuntos, pk=pk)
    if request.method == "POST":
        assunto.delete()
        messages.add_message(request, messages.SUCCESS, 'Assunto excluído com sucesso!')
        return redirect('contas:assuntos')
    context = {'assunto': assunto}
    return render(request, 'contas/apagar_assunto.html', context)


@login_required
@permission_required('contas.delete_questoes', raise_exception=True)
def apagarquestao(request, pk):
    item_questao = Questoes.objects.get(id=pk)
    if request.method == "POST":
        item_questao.delete()
        return redirect('contas:questoes')
    context = {'item_questao': item_questao}
    return render(request, 'contas/apagar_questao.html', context)


@login_required
@permission_required('contas.delete_respostas', raise_exception=True)
def apagarresposta(request, pk):
    item_resposta = Questoes.objects.get(id=pk)
    if request.method == "POST":
        item_resposta.delete()
        return redirect('contas:respostas')
    context = {'item_resposta': item_resposta}
    return render(request, 'contas/apagar_resposta.html', context)

@login_required
@permission_required('contas.delete_questoesorganizationutint', raise_exception=True)
def apagaratribuicao(request, pk):
    questao_organization_unit = QuestoesOrganizationunit.objects.get(id=pk)
    if request.method == "POST":
        questao_organization_unit.delete()
        return redirect('contas:form_atribuicao', pk=questao_organization_unit.questao_id)
    context = {'questao_organization_unit': questao_organization_unit}
    return render(request, 'contas/apagar_atribuicao.html', context)

@login_required
@permission_required('contas.change_categorias', raise_exception=True)
def editarcategoria(request, pk):
    categoria = get_object_or_404(Categorias, id=pk)
    form = CategoriasForm(request.POST or None, instance=categoria)
    if form.is_valid():
        form.save()
        messages.add_message(request, messages.SUCCESS, 'Categoria editada com sucesso!')
        return redirect('contas:categorias')

    context = {'form': form, 'categoria': categoria}
    return render(request, 'contas/form_categoria.html', context)

@login_required
@permission_required('contas.change_assuntos', raise_exception=True)
def editarassunto(request, pk):
    assunto = get_object_or_404(Assuntos, id=pk)
    form = AssuntosForm(request.POST or None, instance=assunto)
    if form.is_valid():
        form.save()
        messages.add_message(request, messages.SUCCESS, 'Assunto editado com sucesso!')
        return redirect('contas:assuntos')

    context = {'form': form, 'assunto': assunto}
    return render(request, 'contas/form_assunto.html', context)


@login_required
@permission_required('contas.change_questoes', raise_exception=True)
def editarquestao(request, pk):
    if request.method == 'POST':
        questao_editado = get_object_or_404(Questoes, id=pk)
        form_quest_editado = QuestoesForm(request.POST, instance=questao_editado)
        if form_quest_editado.is_valid():
            form_quest_editado.save()
        return redirect('contas:questoes')
    else:
        questao_editado = Questoes.objects.filter(id=pk).values().last()
        form_quest_editado = QuestoesForm(initial=questao_editado)
    return render(request, 'contas/editar_questao.html', {'form_quest_editado': form_quest_editado, 'questao_editado': questao_editado})

@login_required
@permission_required('contas.change_respostas', raise_exception=True)
def editarresposta(request, pk):
    if request.method == 'POST':
        resposta_editada = get_object_or_404(
            Respostas, 
            id=pk, 
            organization_unit=request.user.organization_units.all()[0]
        )
        form_resp_editado = RespostasForm(
            request.POST, instance=resposta_editada)
        if form_resp_editado.is_valid():
            form_resp_editado.save()
        return redirect('contas:respostas')
    else:
        resposta_editada = Respostas.objects.filter(
            id=pk,
            organization_unit=request.user.organization_units.all()[0]
        ).values().last()
        form_resp_editado = RespostasForm(initial=resposta_editada)
    return render(request, 'contas/editar_resposta.html', {'form_resp_editado': form_resp_editado, 'resposta_editada': resposta_editada})
