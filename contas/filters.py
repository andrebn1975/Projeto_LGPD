import django_filters
from .forms import RespostasFilterForm, QuestoesFilterForm
from .models import Respostas, Questoes

class RespostasFilter(django_filters.FilterSet):

    class Meta:
        model = Respostas
        form = RespostasFilterForm
        fields = ('organization_unit',)

class QuestoesFilter(django_filters.FilterSet):

    class Meta:
        model = Questoes
        form = QuestoesFilterForm
        fields = ('questao',)