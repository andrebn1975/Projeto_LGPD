from django import forms
from django.forms import widgets

from .models import Categorias, Assuntos, Questoes, Respostas, QuestoesOrganizationunit
from auth_usermanager.models import OrganizationUnits


class CategoriasForm(forms.ModelForm):
    InputTextWidget = forms.Textarea(attrs={'class': 'form-control'})
    categoria = forms.CharField(required=True, widget=InputTextWidget)
    
    class Meta:
        model = Categorias
        fields = '__all__'

class AssuntosForm(forms.ModelForm):
    TextareaWidget = forms.Textarea(attrs={'class': 'form-control'})
    assunto = forms.CharField(required=True, widget=TextareaWidget)
    
    class Meta:
        model = Assuntos
        fields = '__all__'

class QuestoesForm(forms.ModelForm):

    class Meta:
        model = Questoes
        fields = ('assunto', 'questao')

class QuestoesOrganizationunitForm(forms.ModelForm):
    
    class Meta:
        model = QuestoesOrganizationunit
        fields = ('organization_unit',)

class RespostasForm(forms.ModelForm):
    
    TextareaWidget = forms.Textarea(attrs={'class': 'form-control'})
    
    resposta = forms.CharField(required=True, widget=TextareaWidget)
    comentario = forms.CharField(required=True, widget=TextareaWidget)
    revisado = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'id': 'revisado',
                'class': 'form-check-input'
            }
        )
    )

    class Meta:
        model = Respostas
        fields = (
            'questao',
            'resposta', 
            'comentario', 
            'revisado'           
        )
        widgets = {
            'questao': forms.HiddenInput(),
        }


class RespostasForm_Comite(forms.ModelForm):

    class Meta:
        model = Respostas
        fields = ('comentario_cieg', 'revisado')

class RespostasForm_Usuario_Padrao(forms.ModelForm):

    class Meta:
        model = Respostas
        fields = ('resposta', 'questao')

class RespostasFilterForm(forms.ModelForm):

    class Meta:
        model = Respostas
        fields = ('organization_unit',)

    def __init__(self, *args, **kwargs):
        super(RespostasFilterForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control m-2'

class QuestoesFilterForm(forms.ModelForm):

    class Meta:
        model = Questoes
        fields = ('questao',)

    def __init__(self, *args, **kwargs):
        super(QuestoesFilterForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control m-2'

