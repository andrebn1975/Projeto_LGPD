from django.db import models
from auth_usermanager.models import OrganizationUnits



class Categorias(models.Model):
    categoria = models.CharField('Categoria', max_length=255)
    data_criacao = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'Categorias'
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categoria'

    def __str__(self):
        return self.categoria

    def get_assuntos(self):
        return Assuntos.objects.filter(categoria=self)


class Assuntos(models.Model):
    assunto = models.CharField('Assunto', max_length=255)
    categoria = models.ForeignKey(
        Categorias,
        on_delete=models.CASCADE,
        null=False,
        blank=False
    )
    data_criacao = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'Assuntos'
        verbose_name = "Assunto"
        verbose_name_plural = "Assuntos"

    def __str__(self):
        return self.assunto

    def get_questoes(self):
        return Questoes.objects.filter(assunto=self)

class Questoes(models.Model):
    assunto = models.ForeignKey(
        Assuntos,
        on_delete=models.CASCADE,
        null=False,
        blank=False
    )
    organization_units = models.ManyToManyField(
        OrganizationUnits,
        related_name='questoes_set',
        related_query_name='questoes',
        through='QuestoesOrganizationunit',
        through_fields=('questao', 'organization_unit'),
    )
    questao = models.TextField()
    data_criacao = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'Questoes'
        verbose_name = "Questão"
        verbose_name_plural = "Questões"

    def __str__(self):
        return self.questao

    def get_respostas(self):
        return Respostas.objects.filter(questao=self)

class QuestoesOrganizationunit(models.Model):
    questao = models.ForeignKey(
        Questoes,
        on_delete=models.CASCADE,
        db_constraint=False,
        db_column='questao_id',
    )
    organization_unit = models.ForeignKey(
        OrganizationUnits,
        on_delete=models.DO_NOTHING,
        db_constraint=False,
        db_column='organization_unit_id',
    )
    ano_pesquisa = models.IntegerField()
    data_criacao = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'Questoes_OrganizationUnit'


class BaseResposta(models.Model):
    
    CONFORMIDADE_CHOICES = [
        ('Sim', 'Sim'),
        ('Não', 'Não'),
        ('Em Análise', 'Em Análise'),
        ('Considerações do Setor', 'Considerações do Setor'),
        ('Não Aplicável', 'Não Aplicável'),

    ]

    DESDOBRAMENTO_CHOICES = [
        ('Esclareceu', 'Esclareceu'),
        ('Não Esclareceu', 'Não Esclareceu')
    ]

    respondeu = models.BooleanField()
    conformidade = models.CharField(
        choices=CONFORMIDADE_CHOICES, 
        max_length=50, 
        blank=True, 
        null=True
    )
    desdobramento = models.CharField(
        choices=DESDOBRAMENTO_CHOICES, 
        max_length=50, 
        blank=True, 
        null=True
    )

    data_criacao = models.DateField(auto_now_add=True)
    revisado = models.BooleanField()

    def __str__(self):
        return f'{self.resposta[0:10]}...'

    class Meta:
        abstract = True


class Respostas(BaseResposta):
    
    organization_unit = models.ForeignKey(
        OrganizationUnits,
        verbose_name='Órgãos',
        on_delete=models.DO_NOTHING,
        db_constraint=False,
        db_column='organization_unit_id'
    )
    matricula = models.CharField(max_length=10, blank=True, null=True)
    questao = models.ForeignKey(
        Questoes,
        on_delete=models.DO_NOTHING,
    )
    resposta = models.TextField(blank=True, null=True)
    comentario_cieg = models.TextField(blank=True, null=True)
    comentario_comite = models.TextField(blank=True, null=True)
    
    class Meta:
        db_table = 'Respostas'
        verbose_name = 'Resposta'
        verbose_name_plural = 'Respostas'


class Desdobramentos(BaseResposta):

    resposta = models.ForeignKey(
        Respostas,
        on_delete=models.CASCADE,
    )
    parent = models.ForeignKey(
        'self', 
        on_delete=models.CASCADE,
        null=True
    )
    pergunta_adicional = models.TextField(blank=True, null=True)
    resposta_adicional = models.TextField('Resposta Adicional', null=True, blank=True)
    comentario_adicional = models.TextField('Comentário Adicional', null=True, blank=True)

    class Meta:
        db_table = 'Desdobramentos'
        verbose_name = 'Desdobramento'
        verbose_name_plural = 'Desdobramentos'


class TipoRiscos(models.Model):
    tipo_risco = models.CharField('Tipo Risco', max_length=255)
    data_criacao = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'TipoRiscos'
        verbose_name = 'Tipo de Risco'
        verbose_name_plural = 'Tipos de Riscos'

    def __str__(self):
        return self.tipo_risco

    def get_assuntos(self):
        return Riscos.objects.filter(tipo_risco=self)

class Riscos(models.Model):

    ACAO_CHOICES = [
        ('Procedimental', 'Procedimental'),
        ('Tecnológica', 'Tecnológica'),
        ('Procedimental e Tecnológica', 'Procedimental e Tecnológica'),
        ('Aprofundar Entendimento', 'Aprofundar Entendimento')
    ]

    INSTRUMENTO_CHOICES = [
        ('Plano de Ação', 'Plano de Ação'),
        ('Resolução', 'Resolução'),
        ('Resolução e Plano de Ação', 'Resolução e Plano de Ação')
    ]

    ORIGEM_RISCO_CHOICES = [
        ('Resposta Original', 'Resposta Original'),
        ('Desdobramento', 'Desdobramento')
    ]

    origem_risco = models.CharField(choices=ORIGEM_RISCO_CHOICES, max_length=50)
    resposta = models.ForeignKey(
        Respostas,
        on_delete=models.CASCADE,
        null=True
    )

    desdobramento = models.ForeignKey(
        Desdobramentos,
        on_delete=models.CASCADE,
        null=True
    )
    tipo_risco = models.ForeignKey(
        TipoRiscos,
        on_delete=models.CASCADE,
    )
    
    acao = models.CharField(choices=ACAO_CHOICES, max_length=50)
    instrumento = models.CharField(choices=INSTRUMENTO_CHOICES, max_length=50)
    apontamento = models.TextField('Apontamento', blank=True, null=True)
    resolucao = models.TextField('Resolução', null=True, blank=True)
    data_criacao = models.DateField(auto_now_add=True)

    def __str__(self):
        return f'{self.apontamento[0:10]}...'

    class Meta:
        db_table = 'Riscos'
        verbose_name = 'Risco'
        verbose_name_plural = 'Riscos'

class Acao(models.Model):

    ACAO_CHOICES = [
        ('Procedimental', 'Procedimental'),
        ('Tecnológica', 'Tecnológica'),
        ('Procedimental e Tecnológica', 'Procedimental e Tecnológica'),
        ('Aprofundar Entendimento', 'Aprofundar Entendimento')
    ]

    ORGAO_RESPONSAVEL_CHOICES = [
        ('Comitê', 'Comitê'),
        ('STI', 'STI'),
        ('Comitê e STI', 'Comitê e STI')
    ]

    ESTIMATIVA_IMPLEMENTACAO_CHOICES = [
        ('Semestre 2 - 2021', 'Semestre 2 - 2021'),
        ('Semestre 1 - 2022', 'Semestre 1 - 2022'),
        ('Semestre 2 - 2022', 'Semestre 2 - 2022'),
        ('Semestre 1 - 2023', 'Semestre 1 - 2023'),
        ('Semestre 2 - 2023', 'Semestre 2 - 2023')

    ]

    STATUS_ACAO = [
        ('Não Iniciado', 'Não Iniciado'),
        ('Em Andamento', 'Em Andamento'),
        ('Realizado', 'Realizado')
    ]

    risco = models.ForeignKey(
        Riscos,
        related_name='acao_risco',
        on_delete=models.CASCADE,
    )
    acao = models.CharField(choices=ACAO_CHOICES, max_length=50)
    plano_acao = models.TextField('Plano de Ação', null=True, blank=True)
    matriz_atribuicao = models.TextField('Matriz de Atribuição', null=True, blank=True)
    orgao_responsavel = models.CharField(choices=ORGAO_RESPONSAVEL_CHOICES, max_length=50)
    organization_unit = models.ForeignKey(
        OrganizationUnits,
        verbose_name='Órgão Interno',
        on_delete=models.DO_NOTHING,
        db_constraint=False,
        db_column='organization_unit_id',
        null=True,
        blank=True
    )
    matricula = models.CharField(max_length=10, blank=True, null=True) 
    estimativa_implementacao = models.CharField(
        choices=ESTIMATIVA_IMPLEMENTACAO_CHOICES, 
        max_length=50, 
        null=True, 
        blank=True
    )
    status_acao = models.CharField(choices=STATUS_ACAO, max_length=50)
    acompanhamento_acao = models.TextField('Acompanhamento da Ação', null=True, blank=True)

    class Meta:
        db_table = 'Acao'
        verbose_name = 'Ação'
        verbose_name_plural = 'Ações'

    def __str__(self):
        return f'{self.plano_acao[0:10]}...'