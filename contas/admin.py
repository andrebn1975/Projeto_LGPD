from django.contrib import admin
from .models import Assuntos, Questoes, Respostas


admin.site.register(Assuntos)
admin.site.register(Questoes)
admin.site.register(Respostas)

