from django.urls import path
from contas import views

app_name = 'contas'

urlpatterns = [
    path('', views.home, name='home'),
    path('categorias/', views.categorias, name='categorias'),
    path('assuntos/', views.assuntos, name='assuntos'),
    path('questoes/', views.questoes, name='questoes'),
    path('respostas/', views.respostas, name='respostas'),
    path('atribuicao/', views.atribuicao, name='atribuicao'),
    path('criar_atribuicao/<int:pk>/', views.criaratribuicao, name='form_atribuicao'),
    path('respostas_search/', views.respostas_search, name='respostas_search'),
    path('criar_categoria/', views.criarcategoria, name='criar_categoria'),
    path('criar_assunto/', views.criarassunto, name='criar_assunto'),
    path('criar_questao/', views.criarquestao, name='form_questao'),
    path('criar_resposta/<int:questao_id>/', views.criarresposta, name='form_resposta'),
    path('apagar_categoria/<int:pk>/', views.apagarcategoria, name='apagar_categoria'),
    path('apagar_assunto/<int:pk>/', views.apagarassunto, name='apagar_assunto'),
    path('apagar_questao/<int:pk>/', views.apagarquestao, name='apagar_questao'),
    path('apagar_resposta/<int:pk>/', views.apagarresposta, name='apagar_resposta'),
    path('apagar_atribuicao/<int:pk>/', views.apagaratribuicao, name='apagar_atribuicao'),
    path('editar_categoria/<int:pk>/', views.editarcategoria, name='editar_categoria'),
    path('editar_assunto/<int:pk>/', views.editarassunto, name='editar_assunto'),
    path('editar_questao/<int:pk>/', views.editarquestao, name='editar_questao'),
    path('editar_resposta/<int:pk>/', views.editarresposta, name='editar_resposta'),
]
